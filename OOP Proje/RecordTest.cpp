#include "Record.h"
using namespace std;

/**
* @file RecordTest.cpp
* @Author Mustafa Harun Türkmenoğlu
* @date January 04, 2021
* @brief Test Code for Record Class
*/
int main() {

	string fileName;
	cout << "Enter file name:";
	cin >> fileName;
	Record p(fileName);
	if (p.openFile()) {
		cout << "File is opened......" << endl;
	}
	cout << "writing to file...\n";
	p.writeLine("First line test!\n");
	p.writeLine("Second line test!\n");
	p.writeLine("Third line test!\n");

	if (p.closeFile()) {
		cout << "File is closed......" << endl;
	}
	p.writeLine("This line has to be skipped!\n");
	p.openFile();
	p.writeLine("4th line test!\n");
	if (p.closeFile()) {
		cout << "File is closed......" << endl;
	}
	p << "This line has to be skipped!";
	p.openFile();
	p << "5th line test!";

	cout << p.readLine();
	cout << "+-----------------------+" << endl;
	cout << "Operator reading" << endl;
	p >> fileName;

	return 0;
}