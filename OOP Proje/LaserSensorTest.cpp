#include <iostream>
#include "LaserSensor.h"
using namespace std;

/**
* @file LaserSensor.cpp
* @Author Mustafa Batuhan Bayoglu
* @date 5/01/2021
* @brief It is the main function that tests LaserSensor class.
*/
int main() {

	PioneerRobotAPI* rob = new PioneerRobotAPI();
	LaserSensor sensor(rob);
	float ranges[181] = { 1,2,3,4,5,6,7,8,15,10,11,12,13,14,0,2,3,4,5,6,7,8,15,10,11,12,13,14,1,2,3,4,5,6,7,8,15,10,11,12,13,14,1,2,3,4,5,6,7,8,15,10,11,12,13,14,1,2,3,4,5,6,7,8,15,10,11,12,13,14,
		1,2,3,4,5,6,7,8,15,10,11,12,13,14,1,2,3,4,5,6,7,8,15,10,11,12,13,14,1,2,3,4,5,6,7,8,15,10,11,12,13,14,1,2,3,4,5,6,7,8,15,10,11,12,13,14,1,2,3,4,5,6,7,8,15,10,11,12,13,14,1,2,3,4,5,6,7,8,15,10,11,12,13,14,1,2,3,4,5,6,7,8,15,10,11,12,13,14,12 };
	sensor.updateSensor(ranges);
	int a = 5, b = 6;
	float closest;
	float range = sensor.getRange(a);
	float min = sensor.getMin(b);
	float max = sensor.getMax(a);
	float angle = sensor.getAngle(5);
	float closestRange = sensor.getClosestRange(0, 50, closest);
	float oper = sensor[19];

	cout << "Angle of 1. sensor: " << angle << endl;
	cout << "----------------//////////-----------------" << endl;
	cout << "Maximum range among sensors: " << max << endl;
	cout << "Index of maximum value: " << a << endl;
	cout << "----------------//////////-----------------" << endl;
	cout << "Minimum range among sensors: " << min << endl;
	cout << "index of  minimum value: " << b << endl;
	cout << "----------------//////////-----------------" << endl;
	cout << "Range of 120. sensor: " << range << endl;
	cout << "----------------//////////-----------------" << endl;
	cout << "Closest range between 0 and 50 is:" << closestRange << endl;
	cout << "Index of closestRange between 0 and 50 is:" << closest << endl;
	cout << "----------------//////////-----------------" << endl;
	cout << "operator overload is testing:" << oper << endl;
	cout << endl;
	system("pause");
}