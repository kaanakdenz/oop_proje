﻿#include "LaserSensor.h"
#include <iostream>
/**
* @file LaserSensor.h
* @Author Mustafa Batuhan Bayoglu
* @date 05/01/2021
* @brief LaserSensor for recording and showing ranges
*/

//! A destructor.
LaserSensor::~LaserSensor() {}

/*!
\param index bir integer argument.
\return range
*/
float LaserSensor::getRange(int i) const
{
	return this->ranges[i];
}

/*!
\param range  array bir float argument.
\return null
*/
void  LaserSensor::updateSensor(float range[])
{
	for (int i = 0; i < 181; i++) {
		this->ranges[i] = range[i];
	}
}

/*!
\param index bir integer argument.
\return max range
*/
float LaserSensor::getMax(int& index)
{
	int i = 0;
	float max = this->ranges[i];
	for (int i = 0; i <= 181; i++)
	{
		if (this->ranges[i] > max) {
			max = this->ranges[i];
			index = i;
		}
	}
	return max;
}

/*!
\param index bir integer argument.
\return min range
*/
float LaserSensor::getMin(int& index)
{
	int i = 0;
	float min = this->ranges[i];
	for (int i = 0; i <= 181; i++)
	{
		if (min > this->ranges[i]) {
			min = this->ranges[i];
			index = i;
		}
	}
	return min;
}

/*!
\param index bir Pose argument.
\return range
*/
float LaserSensor::operator[](int index)
{

	if (0 <= index && index <= 181) {
		return this->ranges[index];
	}
	else return -1;
}

/*!
\param index bir integer argument.
\return angle
*/
float LaserSensor::getAngle(int index)
{
	if (index == 0 || index == 8) {
		return 90;
	}
	else if (index == 1 || index == 9) {
		return 50;
	}
	else if (index == 2 || index == 10) {
		return 30;
	}
	else if (index == 3 || index == 11) {
		return 10;
	}
	else if (index == 4 || index == 12) {
		return -10;
	}
	else if (index == 5 || index == 13) {
		return -30;
	}
	else if (index == 6 || index == 14) {
		return -50;
	}
	else if (index == 7 || index == 15) {
		return -90;
	}
	else {
		return -1;
	}
}

/*!
\param startAngle bir float argument, endAngle bir float argument, angle bir float argument.
\return en yakın range
*/
float LaserSensor::getClosestRange(float startAngle, float endAngle, float& angle)
{

	float minRange = this->ranges[int(startAngle)];
	for (float i = startAngle; i < endAngle; i++)
	{
		if (minRange > this->ranges[int(i)]) {
			minRange = this->ranges[int(i)];
			angle = i;
		}
	}
	return minRange;
}