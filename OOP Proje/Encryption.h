#pragma once
/**
* @file Encryption.h
* @Author Mustafa Harun Türkmenoğlu
* @date January 05, 2021
* @brief Encryption class for Encryption and Decryption
*/
class Encryption {
public:
	int encrypt(int);
	int decrypt(int);
};