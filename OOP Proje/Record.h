#pragma once
#include <iostream>
#include <fstream>
#include <string>
using namespace std;

/**
* @file Record.h
* @Author Mustafa Harun Türkmenoğlu
* @date January 04, 2021
* @brief Record class that operates read and write operations
*/
class Record
{
private:
	string fileName;
	fstream file;
public:
	//!A constructor
	Record(string);
	//!A deconstructor
	~Record();
	//!Checks if file is opened or not
	bool openFile();
	//!Checks if file closed or not
	bool closeFile();
	//!Set fucntion for file name
	void setFileName(string);
	//!Reads line from file
	string readLine();
	//!Writes line to file
	bool writeLine(string);
	//!Overload operator to write to file
	Record& operator<<(string);
	//!Overload operator to read from file
	Record& operator>>(string);
	//!Gets fileName
	string getFileName()const;
};