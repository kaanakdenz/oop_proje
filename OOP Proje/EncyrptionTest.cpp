#include "Encryption.h"
#include <iostream>
using namespace std;

/**
* @file EncryptionTest.cpp
* @Author Mustafa Harun Türkmenoğlu
* @date January 05, 2021
* @brief Encryption Test  
*/

int main() {
	Encryption encyription;

	int a = 7749;
	int b= encyription.encrypt(a);
	b = encyription.decrypt(b);

	if (a == b)
		cout << "encryption ve decryption works right.....";
	else
		cout << "encryption ve decryption works wrong.....";

	return 0;
}