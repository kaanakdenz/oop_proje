#include "RobotOperator.h"
#include <iostream>
using namespace std;

/**
* @file RobotOperatorTest.cpp
* @Author Mustafa Harun Turkmenošlu
* @date January 05, 2021
* @brief RobotOperator class to Authorize Operator
*/

int main() {
	
	//password 7749- Encyrpted 1644
	RobotOperator oper("harun", "turkmenoglu");
	oper.checkAccessCode(1000);
	oper.print();
	cout << endl;
	oper.checkAccessCode(7749);
	oper.print();
	cout << endl;
	cout << "Encryption of 7749: " << oper.callEncrypter(7749) << endl;
	cout << "Decryption of encrypted number: " << oper.callDecrypter(oper.callEncrypter(7749)) << endl;

	return 0;
}