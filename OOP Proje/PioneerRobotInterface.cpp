﻿#include "PioneerRobotInterface.h"

/**
* @file PioneerRobotInterface.h
* @Author Mustafa Batuhan Bayoğlu 152120171002
* @date 24.01.2021
* @brief This file using for control of robot.
*/

/*!
\param null
\return void
*/
void PioneerRobotInterface::turnLeft()
{
	robotAPI->turnRobot(PioneerRobotAPI::DIRECTION::left);
}

/*!
\param null
\return void
*/
void PioneerRobotInterface::turnRight()
{
	robotAPI->turnRobot(PioneerRobotAPI::DIRECTION::right);
}

/*!
\param float parameter
\return void
*/
void PioneerRobotInterface::forward(float speed)
{
	robotAPI->moveRobot(speed);
}

/*!
\param speed isa float argument
\return void
*/
void PioneerRobotInterface::backward(float speed)
{
	robotAPI->moveRobot(-speed);
}

/*!
\param null.
\return void
*/
void PioneerRobotInterface::print()
{
	Pose* pose;
	pose = getPose();
	cout << "pose is (" << pose->getX() << "," << pose->getY() << "," << pose->getTh() << ")" << endl;
}

/*!
\param null.
\return Pose
*/
Pose* PioneerRobotInterface::getPose()
{
	position = new Pose(robotAPI->getX(), robotAPI->getY(), robotAPI->getTh());
	setPose(position);
	return this->position;
}

/*!
\param pose is Pose argument.
\return void
*/
void PioneerRobotInterface::setPose(Pose* pose)
{
	this->position = pose;
}

/*!
\param null.
\return void
*/
void PioneerRobotInterface::stopTurn()
{
	robotAPI->stopRobot();
}
/*!
\param null.
\return void
*/
void PioneerRobotInterface::stopMove()
{
	robotAPI->stopRobot();
}

/*!
\param null.
\return void
*/
void PioneerRobotInterface::updateSensors()
{
	float ranges[181] = { 1,2,3,4,5,6,7,8,15,10,11,12,13,14,0,2,3,4,5,6,7,8,15,10,11,12,13,14,1,2,3,4,5,6,7,8,15,10,11,12,13,14,1,2,3,4,5,6,7,8,15,10,11,12,13,14,1,2,3,4,5,6,7,8,15,10,11,12,13,14,
		1,2,3,4,5,6,7,8,15,10,11,12,13,14,1,2,3,4,5,6,7,8,15,10,11,12,13,14,1,2,3,4,5,6,7,8,15,10,11,12,13,14,1,2,3,4,5,6,7,8,15,10,11,12,13,14,1,2,3,4,5,6,7,8,15,10,11,12,13,14,1,2,3,4,5,6,7,8,15,10,11,12,13,14,1,2,3,4,5,6,7,8,15,10,11,12,13,14,12 };
	sensor->updateSensor(ranges);
}
