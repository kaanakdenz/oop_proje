#pragma once
#include"Node.h"
#include<iostream>
using namespace std;
/**
* @file Path.h
* @Author Yasemin Gerboga 152120171056
* @date 3/01/2021
* @brief It is the class that allows managing the road plan in the Linked list form.
*/
class Path
{
private:
	Node* tail;
	Node* head;
	int number;
public:
	//! A constructor.
	Path();
	//! A destructor.
	~Path();
	//! adding Pose in the end of the list
	void addPose(Pose);
	//! print all list
	void print() const;
	//! getting ..th index of the list.
	Pose& operator[](int) const;
	//! Get position in the spesific index.
	Pose getPose(int)const;
	//! remove position in the spesific index.
	bool removePos(int);
	//! insert position in the spesific index.
	bool insretPos(int, Pose);
	//! Adds the position entered from the keyboard to the end of the list.
	void operator>>(Pose&);
	//! print all list
	void operator<<(Node*);
};