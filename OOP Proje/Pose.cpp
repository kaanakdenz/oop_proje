﻿#define _USE_MATH_DEFINES
#include "Pose.h"
#include<math.h>
# define M_PI           3.14159265358979323846  /* pi */
/**
* @file Pose.h
* @Author Mustafa Batuhan Bayoğlu 152120171002
* @date  02/01/2021
* @brief This file using for control of robot position.
*/

//! A constructor.
Pose::Pose(float _x , float _y , float _z )
{
	this->setX(_x);
	this->setY(_y);
	this->setTh(_z);
}
//! A destructor.
Pose::~Pose()
{
}

/*!
\param null.
\return x koordinat pozisyonu
*/
float Pose::getX()const
{
	return this->x;
}
/*!
\param x bir float argument.
\return null
*/
void Pose::setX(float x)
{
	this->x = x;
}

/*!
\param null.
\return y koordinat pozisyonu
*/
float Pose::getY()const
{
	return this->y;
}
/*!
\param y bir float argument.
\return null
*/
void Pose::setY(float y)
{
	this->y = y;
}
/*!
\param null.
\return aci
*/
float Pose::getTh()const
{
	return this->th;
}
/*!
\param th bir float argument.
\return null
*/
void Pose::setTh(float _th)
{
	(_th > 360) ? this->th = fmod(_th,360) : this->th = _th;
}

/*!
\param other bir Pose argument.
\return esit yada esit degil esitsizligi
*/
bool Pose::operator==(const Pose& other)
{
	return (this->x == other.x && this->y == other.y && this->th == other.th);
}
/*!
\param other bir Pose argument.
\return eklenmis pozisyon
*/
Pose Pose::operator+(const Pose& other)
{
	Pose position;
	position.x = this->x + other.x;
	position.y = this->y + other.y;
	position.th = this->th + other.th;

	return position;
}

/*!
\param other bir Pose argument.
\return cikarilmis pozisyon
*/
Pose Pose::operator-(const Pose& other)
{
	Pose position;
	position.x = this->x - other.x;
	position.y = this->y - other.y;
	position.th = this->th - other.th;

	return position;
}

/*!
\param other bir Pose argument.
\return üzerine eklenmis pozisyon
*/
Pose& Pose::operator+=(const Pose& other)
{
	this->x = this->x + other.x;
	this->y = this->y + other.y;
	this->th = this->th + other.th;

	return *this;
}
/*!
\param other bir Pose argument.
\return uzerınden cikarilmis pozisyon
*/
Pose& Pose::operator-=(const Pose& other)
{
	this->x = this->x - other.x;
	this->y = this->y - other.y;
	this->th = this->th - other.th;

	return *this;
}
/*!
\param other bir Pose argument.
\return kucuktur veya kucuk degildir esitsizligi
*/
bool Pose::operator<(const Pose& other)
{
	return (this->x < other.x && this->y < other.y && this->th < other.th);
}
/*!
\param null.
\return pozisyon
*/
Pose Pose::getPose()const
{

	return *this;
}
/*!
\param _x bir float argument, _y bir float argument, _th bir float argument.
\return null
*/
void Pose::setPose(float _x, float _y, float _th)
{
	this->x = _x;
	this->y = _y;
	this->th = _th;
}
/*!
\param pos bir Pose argument
\return iki pozisyon arasi uzaklik
*/
float Pose::findDistanceTo(Pose pos)
{
	float xValue = this->x - pos.x;
	float yValue = this->y - pos.y;
	return sqrt(pow(xValue, 2) + pow(yValue, 2));
}
/*!
\param pos bir Pose argument
\return iki pozisyon arasi aci
*/
float Pose::findAngleTo(Pose pos)
{
	float angle = atan2(this->y - pos.y, this->x - pos.x);
	if(pos.x < this->x && pos.y < this->y || pos.x > this->x && pos.y < this->y){
		angle = (angle * 180 / M_PI);
	}
	else
	{
		angle = (angle * 180 / M_PI) + 360;
	}
	return angle;
}