#include"Pose.h"

using namespace std;

/**
* @file PathTest.cpp
* @Author Mustafa Batuhan Bayoglu 152120171002
* @date 3/01/2021
* @brief It is the main function that tests Pose class.
*/
int main() {

	Pose posA(7,8,30);
	Pose posB(30);
	Pose posC;
	bool temp;
	posB.setX(3);
	posB.setY(5.196);
	posB.setTh(0);
	cout << "------------------Aftrer initial assign------------------------------\n" << endl;

	cout << "Position A -->\tx:" << posA.getX() << "\ty:" << posA.getY() << "\tth:" << posA.getTh() <<endl;
	cout << "Position B -->\tx:" << posB.getX() << "\ty:" << posB.getY() << "\tth:" << posB.getTh() << endl;
	cout << "Position C -->\tx: " << posC.getX() << "\ty: " << posC.getY() << "\tth: " << posC.getTh() << endl;
	posA.setPose(0, 0, 60);
	posC.setX(10);
	posC.setY(10);
	posC.setTh(45);
	cout << "\n-------------------------Aftrer  update------------------------------\n" << endl;

	cout << "Position A -->\tx:" << posA.getX() << "\ty:" << posA.getY() << "\tth:" << posA.getTh() << endl;
	cout << "Position C -->\tx: " << posC.getX() << "\ty: " << posC.getY() << "\tth: " << posC.getTh() << endl;

	cout << "\n------------------------- Checking '<' operator  ------------------------------\n" << endl;

	temp = posA < posB;
	(temp == true) ? cout << "position A is smaller than position B\n" : cout << "position A is smaller than position B\n";

	cout << "\n------------------------- Checking '+=' operator  ------------------------------\n" << endl;
	cout << "posC += posB" << endl;
	posC += posB;
	cout << "Position C -->\tx:" << posC.getX() << "\ty:" << posC.getY() << "\tth:" << posC.getTh() << endl;

	cout << "\n------------------------- Checking '+' operator  ------------------------------\n" << endl;
	cout << "posC = posA + posB" << endl;
	posC = posA + posB;
	cout << "Position C -->\tx:" << posC.getX() << "\ty:" << posC.getY() << "\tth:" << posC.getTh() << endl;

	cout << "\n-------------------------  Checking  distance and angle ------------------------------\n" << endl;

	cout << "Position A -->\tx:" << posA.getX() << "\ty:" << posA.getY() << "\tth:" << posA.getTh() << endl;
	cout << "Position B -->\tx:" << posB.getX() << "\ty:" << posB.getY() << "\tth:" << posB.getTh() << endl;

	cout << "PosA's distance to PosB is : " << posA.findDistanceTo(posB) << endl;
	cout << "PosB's angle to PosA is : " << posB.findAngleTo(posA) << endl;

	cout << "\n-------------------------  end  ------------------------------\n" << endl;
	system("pause");
}