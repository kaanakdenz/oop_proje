#include "PioneerRobotAPI.h"
/**
* @file LaserSensor.h
* @Author Mustafa Harun Turkmenoglu
* @Date 24.01.2021
* @brief RangeSensor for creating an abstract class which makes another classes addable
*/
class RangeSensor
{
private:
	PioneerRobotAPI *robotAPI;
public:
	~RangeSensor();
    //a getter function is not written
	float ranges[181];
	virtual float getRange(int) const=0;
	virtual float getMax(int&)=0;
	virtual float getMin(int&)=0;
	virtual void updateSensor(float[])=0;
	virtual float operator[](int)=0;
	virtual float getAngle(int)=0;
	virtual float getClosestRange(float, float, float&)=0;
};