#include "Encryption.h"
using namespace std;

/**
* @file Encryption.cpp
* @Author Mustafa Harun Türkmenoğlu
* @date January 05, 2021
* @brief Encryption class for Encryption and Decryption
*/

/**
* \brief Encrypt given 4-digits number
* \param :integer
* \brief returns encrypted number
*/
int Encryption::encrypt(int number)
{
	int temp = 15, n = 1000, num = 0;
	int basamak = 4, encryption;
	int a, b, c, d;

	//encript code
	while (basamak > 0)
	{
		temp = (number / n) % 10;
		temp = (temp + 7) % 10;
		num += (temp * n);
		n /= 10;
		basamak--;
	}
	a = (num / 1000);
	b = ((num / 10) % 10);
	c = (num / 100) % 10;
	d = (num % 10);

	a = a * 10;
	b = b * 1000;
	c = c * 1;
	d = d * 100;

	encryption = a + b + c + d;
	return encryption;
}

/**
* \brief Decrypt given 4-digits number
* \param :integer
* \brief returns decrypted number
*/
int Encryption::decrypt(int number)
{
	int bas = 4, num2, temp, temp2;
	int bas2 = 4, l = 1000, decryption = 0;
	int e, f, g, h;
	temp = number;

	e = (temp / 1000);
	f = ((temp / 10) % 10);
	g = (temp / 100) % 10;
	h = (temp % 10);

	e = e * 10;
	f = f * 1000;
	g = g * 1;
	h = h * 100;

	num2 = e + f + g + h;

	while (bas2 > 0)
	{
		temp2 = (num2 / l) % 10;
		temp2 = (temp2 + 3) % 10;
		decryption += (temp2 * l);
		l /= 10;
		bas2--;
	}
	return decryption;
}
