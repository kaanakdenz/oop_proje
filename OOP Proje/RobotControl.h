#ifndef ROBOTCONTROL_H_
#define ROBOTCONTROL_H_

#include"Path.h"
#include"Record.h"
#include"RobotOperator.h"
#include"PioneerRobotInterface.h"



/**
* @file RobotControl.h v2
* @Author Kaan Akdeniz 152120161031
* @date 26.12.2018
* @brief This file using for control of robot.
*/

class RobotControl
{

private:
	Path* path = new Path();
	Record* record = new Record("Path");
	RobotOperator* password = new RobotOperator("admin", "admin");
	PioneerRobotAPI* robot= new PioneerRobotAPI();
	RobotInterface* intface = new PioneerRobotInterface(robot);
	int posCount = 1;
	bool pass;

public:
	//! RobotControl Constructor. It takes main robot with parameter.
	RobotControl(PioneerRobotAPI* robot) {
		this->robot = robot;
	}
	//! RobotControl Destructor.
	~RobotControl() {};
	bool addToPath();
	bool clearPath();
	bool recordPathtoFile();
	bool openAccess(int);
	bool closeAccess(int);
	//! Turn robot to the left.
	void turnLeft();
	//! Turn robot to the right.
	void turnRight();
	//! Move forward.
	void forward(float speed);
	//! Move backward.
	void backward(float speed);
	//! Print the robot position.
	void print();
	//! Get the position of robot.
	Pose getPose();
	//! Set the position of robot.
	void setPose(Pose);
	//! Stop turning.
	void stopTurn();
	//! Stop moving.
	void stopMove();
};
#endif