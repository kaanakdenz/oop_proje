#pragma once
#include"Pose.h"
/**
* @file Node.h
* @Author Yasemin Gerboga 152120171056
* @date 2/01/2021
* @brief This file using for Node list
*/
class Node
{
public:
	//! A constructor.
	Node();
	//! A destructor.
	~Node();
	//! next pointer for Node
	Node* next;
	//! pose Pose
	Pose pose;
};