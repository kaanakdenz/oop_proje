#include"Path.h"
#include<iostream>
using namespace std;
/**
* @file PathTest.cpp
* @Author Yasemin Gerboga 152120171056
* @date 3/01/2021
* @brief It is the main function that tests Path class.
*/
int main()
{
	Path path;
	Node node1, node2, node3, node4, node5, node6;
	path.print();
	path.removePos(1);
	node1.pose.setPose(1, 2, 3);
	path.addPose(node1.pose);
	path.removePos(2);
	node2.pose.setPose(4, 5, 6);
	path.addPose(node2.pose);
	path.removePos(0);
	node3.pose.setPose(7, 8, 9);
	path.addPose(node3.pose);
	node4.pose.setPose(10, 11, 12);
	path.addPose(node4.pose);
	path.print();
	cout << "Erase pose in the second index in the list. (List first element is in 0 index.)" << endl;
	path.removePos(2);
	path.print();
	cout << "Inserting in the second index " << endl;
	node5.pose.setPose(9, 9, 9);
	path.insretPos(2, node5.pose);
	path.print();
	cout << endl << "3. Pose" << endl;
	Pose pose = path.getPose(3);
	cout << "path 3:" << " x: " << pose.getX() << " y: " << pose.getY() << " th: " << pose.getTh() << endl;
	cout << "path[2]: " << " x: " << path[2].getX() << " y: " << path[2].getY() << " th: " << path[2].getTh() << endl;
	cout << endl << "Adding the location entered from the keyboard to the end of the list with >> ." << endl;
	Pose newPose;
	path.operator>>(newPose);
	path.print();
	cout << endl << "Printing with << " << endl;
	Node* node = new Node();
	node->pose = path.getPose(1);
	path.operator<<(node);
	return 0;
}