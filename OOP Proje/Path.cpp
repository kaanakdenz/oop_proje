#include "Path.h"
/**
* @file Path.cpp
* @Author Yasemin Gerboga 152120171056
* @date 3/01/2021
* @brief It is the class that allows managing the road plan in the Linked list form.
*/
//! A constructor.
Path::Path()
{
	this->head = this->tail = NULL;
	this->number = 0;
}
//! A destructor.
Path::~Path()
{
	while (this->head != NULL) {
		Node* node = this->head;
		this->head = this->head->next;
		delete node;
	}
	this->number = 0;
	this->tail = NULL;
}
/*!
\param pose bir Pose argument.
\return NULL
*/
void Path::addPose(Pose pose)
{
	Node* node = new Node();
	node->pose = pose;
	node->next = NULL;
	if (head == NULL) {
		head = tail = node;
		this->number++;
	}
	else {
		tail->next = node;
		tail = node;
		this->number++;
	}
}
/*!
\param NULL
\return NULL
*/
void Path::print() const
{
	Node* node = head;
	if (head == NULL) {
		cout << "Empty list!" << endl;
	}
	while (node) {
		cout << "x: " << node->pose.getX() << " y: " << node->pose.getY() << " th: " << node->pose.getTh() << endl;
		node = node->next;
	}
}
/*!
\param index bir integer sayi.
\return NULL
*/
Pose& Path::operator[](int index) const
{
	Pose pose = getPose(index);
	return pose;
}
/*!
\param index bir integer sayi.
\return pose
*/
Pose Path::getPose(int index) const
{
	Node* p = head;
	int i = 1;
	while (p) {
		if (i == index) {
			return p->pose;
		}
		else {
			i++;
			p = p->next;
		}
	}
}
/*!
\param index bir integer sayi.
\return silinip silinmeme durumu
*/
bool Path::removePos(int index)
{
	Node* p = head;
	Node* q = NULL;
	int i;
	if (!head)
	{
		cout << "List is empty! " << endl;
		return false;
	}
	if (index <= 0) {
		cout << "Please enter valid index number! " << endl;
		return false;
	}
	if (this->number < index)
	{
		cout << "List doesn't have " << index << " th element." << endl;
		return false;
	}
	for (i = 1; i < index; i++)
	{
		q = p;
		p = p->next;
	}
	if (head == tail && this->number == 1 && index == 1) //Listede tek eleman varsa ve o silinecekse
	{
		head = NULL;
		return true;
	}
	else if (p->next != NULL && index == 1 && this->number > 1) //Listedeki ilk eleman silinecekse
	{
		head = head->next;
		return true;
	}
	else if (p->next == NULL && index == this->number) // Son eleman silinecekse
	{
		p = head;
		for (i = 0; i < index; i++)
		{
			p = p->next;
		}
		free(p->next);
		p = tail;
		return true;
	}
	else //Ortadan eleman silme
	{
		q->next = p->next;
		free(p);
		return true;
	}
}
/*!
\param index bir integer sayi ve pose bir Pose argument
\return eklenip eklenememe durumu
*/
bool Path::insretPos(int index, Pose pose)
{
	int ind = 1;
	Node* p = head;
	Node* q = NULL;
	Node* newNode = new Node;
	newNode->pose = pose;
	while (p)
	{
		if (index == ind)
		{
			q->next = newNode;
			newNode->next = p;
			return true;
		}
		else
		{
			q = p;
			p = p->next;
			ind++;
		}
	}
}
/*!
\param pose bir Pose argument
\return NULL
*/
void Path::operator>>(Pose& pose)
{
	float x, y, th;
	cin >> x >> y >> th;
	pose.setX(x);
	pose.setY(y);
	pose.setTh(th);
	this->addPose(pose);
}
/*!
\param node bir Node* argument
\return NULL
*/
void Path::operator<<(Node* node)
{
	node = this->head;
	if (!this->head) {
		cout << "Empty list!" << endl;
	}
	while (node) {
		cout << "x: " << node->pose.getX() << " y: " << node->pose.getY() << " th: " << node->pose.getTh() << endl;
		node = node->next;
	}
}