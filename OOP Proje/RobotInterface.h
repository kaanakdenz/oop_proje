﻿#pragma once
#include"Pose.h"
/**
* @file RobotInterface.h
* @Author Yasemin Gerboğa 152120171056
* @date 23.01.2021
* @brief This file has abstract class.
*/
class RobotInterface
{
protected:
	Pose* position;
	int state;
public:
	//! Constructor
	RobotInterface(Pose* pose=NULL,int state=0) {
		this->position = pose;
		this->state = state;
	}
	//! Destructor
	virtual ~RobotInterface() {};
	//! Move robot to left.
	virtual void turnLeft() = 0;	
	//! Move robot to right.
	virtual void turnRight() = 0;
	//! Move robot to forward with speed paramater.
	virtual void forward(float speed) = 0;
	//! Print robot position.
	virtual void print() = 0;
	//! Move robot to backward with speed parameter.
	virtual void backward(float speed) = 0;
	//! Get robot position.
	virtual Pose* getPose() = 0;
	//! Set robot position.
	virtual void setPose(Pose* pose) = 0;
	//! Stop turning.
	virtual void stopTurn() = 0;
	//! Stop moving.
	virtual void stopMove() = 0;
	//! Update sensor values.
	virtual void updateSensors() = 0;
};