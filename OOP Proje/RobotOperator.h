#pragma once
#include <string>
#include "Encryption.h"
using namespace std;
/**
* @file RobotOperator.h
* @Author Mustafa Harun Turkmenošlu
* @date January 05, 2021
* @brief RobotOperator class to Authorize Operator
*/
class RobotOperator
{
private:
	string name;
	string surname;
	Encryption encryption;
	unsigned int accessCode = encryptCode(7749);
	bool accessState;
	int encryptCode(int);
	int decryptCode(int);
public:
	//! A constructer 
	RobotOperator(string _name, string _surname) :name(_name), surname(_surname){}
	//! A Deconstructer
	~RobotOperator();
	//! set name
	void setName(string);
	//! get name
	string getName()const;
	//! set name
	void setSurname(string);
	//! get surname
	string getSurname()const;
	//! set access state
	void setAccessState(bool);
	//! get access state
	bool getAccessState()const;
	//! set access code
	unsigned int getAccessCode();
	//! check access code
	void checkAccessCode(int);
	//! print name,surname,access state
	void print();
	//! calls the encryption function 
	int callEncrypter(int);
	//! calls the decryption function
	int callDecrypter(int);
};
