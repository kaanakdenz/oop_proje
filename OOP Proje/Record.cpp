#include "Record.h"
using namespace std;

/**
* @file Record.cpp
* @Author Mustafa Harun Turkmenoglu
* @date January 04, 2021
* @brief Record class that operates read and write operations
*/

//!A constructor
Record::Record(string _fileName)
{
	setFileName(_fileName);
}

//!A deconstructor
Record::~Record()
{
}

//!Opens File and checks if file is opened or not
bool Record::openFile()
{
	file.open(getFileName(), ios::in | ios::out | ios::app);

	if (this->file.is_open())
		return true;
	else 
		return false;
}
//!Closes File and checks if file closed or not
bool Record::closeFile()
{
	this->file.close();

	if (this->file.is_open())
		return false;
	else {
		return true;
	}
}

//!Sets file name
void Record::setFileName(string _fileName)
{
	this->fileName = _fileName + ".txt";
}

//!Reads lines from file
string Record::readLine()
{
	closeFile();
	string line,sum;
	ifstream myfile(getFileName());
	if (myfile.is_open())
	{
		while (getline(myfile, line))
		{
			//cout << line << '\n';
			sum += line + '\n';
		}
		myfile.close();
	}
	openFile();
	return sum;
}

//!Writes line to file
bool Record::writeLine(string str)
{
	if (this->file.is_open())
	{
		this->file << str;
		return true;
	}
	else
	{
		return false;
	}
}

//!Overload operator to write to file
Record& Record::operator<<(string str)
{
	str += '\n';
	writeLine(str);

	return *this;
}

//!Overload operator to read from file
Record& Record::operator>>(string str)
{
	str += ".txt";
	if (str == this->getFileName())
	{
		string s= readLine();
		cout<< readLine();
	}
	return *this;
}

//!Gets fileName
string Record::getFileName() const
{
	return this->fileName;
}