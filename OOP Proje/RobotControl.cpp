#include "RobotControl.h"
#include<iostream>

using namespace std;

/**
* @file RobotControl.h
* @Author Kaan Akdeniz 152120161031 && Mustafa Harun Turkmenoglu 152120171059
* @brief This file is source file of robot control class.
* This class contains the functionality of controlling to robot.
*/

bool RobotControl::addToPath()
{
	if (pass) {
		path->addPose(*intface->getPose());
		cout << "Current pose added to path!" << endl;
		posCount++;
		return true;
	}
	else {
		cout << "Pose adding process failed!" << endl;
		return false;
	}
}

bool RobotControl::clearPath()
{
	if (pass) {

		for (int i = 0; i < posCount; i++)
			path->removePos(i);
		cout << "All positions removed from path!" << endl;
		return true;
	}
	else {
		cout << "Remove process failed!" << endl;
		return false;
	}
}

bool RobotControl::recordPathtoFile()
{
	if (pass) {
		for (int i = 1; i < posCount; i++) {
			Pose tempPose = path->getPose(i);
			string poseName = to_string(tempPose.getX()) + "," + to_string(tempPose.getY()) + "," + to_string(tempPose.getTh()) + "\n";
			record->openFile();
			record->writeLine(poseName);
			cout << "Path recorded to file!" << endl;
		}
		return true;
	}
	else {
		cout << "Record process failed!" << endl;
		return false;
	}
}

bool RobotControl::openAccess(int state)
{
	password->checkAccessCode(state);
	if (password->getAccessState()) {
		pass = true;
		return true;
	}
	else
	{
		pass = false;
		return false;
	}
}

bool RobotControl::closeAccess(int state)
{
	password->checkAccessCode(state);
	if (password->getAccessState()) {
		pass = false;
		return true;
	}
	else {
		pass = true;
		return false;
	}
}

void RobotControl::turnLeft()
{
	intface->turnLeft();
}

void RobotControl::turnRight()
{
	intface->turnRight();
}

void RobotControl::forward(float speed)
{
	intface->forward(speed);
}

void RobotControl::backward(float speed)
{
	intface->backward(speed);
}

void RobotControl::print()
{
	intface->print();
}

Pose RobotControl::getPose()
{
	return *intface->getPose();
}

void RobotControl::setPose(Pose pos)
{
	intface->setPose(&pos);
}

void RobotControl::stopTurn()
{
	intface->stopTurn();
}

void RobotControl::stopMove()
{
	intface->stopMove();
}
