﻿#pragma once
#include"PioneerRobotAPI.h"
#include"Pose.h"
#include"RobotInterface.h"
#include"LaserSensor.h"
/**
* @file PioneerRobotInterface.h
* @Author Mustafa Batuhan Bayoglu 152120171002
* @date 24.01.2021
* @brief This file using for control of robot.
*/
class PioneerRobotInterface :
	public RobotInterface
{
private:
	PioneerRobotAPI* robotAPI;
	LaserSensor* sensor;
public:
	//! Constructor
	PioneerRobotInterface(PioneerRobotAPI* robot) {
		robotAPI = robot;
	}
	//! Destructor
	~PioneerRobotInterface() {}
	//! Move robot to left.
	void turnLeft();
	//! Move robot to right.
	void turnRight();
	//! Move robot to forward with speed paramater.
	void forward(float speed);
	//! Move robot to backward with speed parameter.
	void backward(float speed);
	//! Print robot position.
	void print();
	//! Get robot position.
	Pose* getPose();
	//! Set robot position.
	void setPose(Pose*);
	//! Stop turning.
	void stopTurn();
	//! Stop moving.
	void stopMove();
	//! Update sensor values.
	void updateSensors();
};

