#include "RobotOperator.h"
#include <iostream>
using namespace std;

/**
* @file RobotOperator.cpp
* @Author Mustafa Harun Turkmenošlu
* @date January 05, 2021
* @brief RobotOperator class to Authorize Operator
*/

/**
* \brief encrypts code
* \param integer
*/
int RobotOperator::encryptCode(int number)
{
	return encryption.encrypt(number);
}

/**
* \brief decrypts code
* \param integer
*/
int RobotOperator::decryptCode(int number)
{
	return encryption.decrypt(number);
}

//! A deconstructer
RobotOperator::~RobotOperator()
{
}

/**
* \brief set name
* \param string
*/
void RobotOperator::setName(string _name)
{
	name = _name;
}

/**
* \brief get name
* \return string
*/
string RobotOperator::getName() const
{
	return this->name;
}

/**
* \brief set surname
* \param string
*/
void RobotOperator::setSurname(string _surname) 
{
	surname = _surname;
}

/**
* \brief get surname
* \return string
*/
string RobotOperator::getSurname() const
{
	return surname;
}

void RobotOperator::setAccessState(bool _accessState)
{
	accessState = _accessState;
}

bool RobotOperator::getAccessState() const
{
	return accessState;
}

unsigned int RobotOperator::getAccessCode()
{
	return accessCode;
}

/**
* \brief get checks both the access code and the given number
* \param integer
*/
void RobotOperator::checkAccessCode(int number)
{
	if (callEncrypter(number) == getAccessCode()) {
		setAccessState(true);
	}
	else {
		setAccessState(false);
	}
}

/**
* \brief print name , surname and access state
* \return string
*/
void RobotOperator::print()
{
	cout << "Name: " << getName() << endl;
	cout << "Surname: " << getSurname() << endl;
	cout << "Access State: " << getAccessState() << endl;
}

int RobotOperator::callEncrypter(int number)
{
	return encryptCode(number);
}

int RobotOperator::callDecrypter(int number)
{
	return decryptCode(number);
}