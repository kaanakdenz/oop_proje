#include<iostream>
#include"RobotControl.h"
#include"PioneerRobotAPI.h"

using namespace std;

/**
* @file RobotControl.h
* @Author Kaan Akdeniz 152120161031
* @brief This file is test file of robot control class.
* This class contains the test of functions that controls the robot.
*/

int main()
{
	PioneerRobotAPI* rob = new PioneerRobotAPI();
	RobotControl* robot = new RobotControl(rob);

	Pose pos;
	pos.setX(5);
	pos.setY(7);
	pos.setTh(30);
	robot->setPose(pos);
	robot->forward(1000);
	robot->turnRight();
	robot->stopTurn();
	robot->backward(500);
	robot->stopMove();
	robot->turnLeft();
	robot->print();

	system("pause");
}