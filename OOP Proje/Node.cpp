#include "Node.h"
/**
* @file Node.cpp
* @Author Yasemin Gerboga 152120171056
* @date 2/01/2021
* @brief This file using for Node list
*/
//! A constructor.
Node::Node()
{
	this->next = NULL;
}
//! A destructor.
Node::~Node()
{
}