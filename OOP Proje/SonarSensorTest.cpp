#include <iostream>
#include "SonarSensor.h"
using namespace std;

/**
* @file RobotControl.h
* @Author Kaan Akdeniz 152120161031
* @brief This file is test file of robot control class.
* This class contains the tests of functions sonar sensor of the robot.
*/

int main() {
	PioneerRobotAPI* rob = new PioneerRobotAPI();
	SonarSensor sensor(rob);
	float ranges[16] = { 3,2,1,4,5,6,7,8,15,3,10,11,12,13,14,8 };
	sensor.updateSensor(ranges);
	int a = 5, b = 6;
	float range = sensor.getRange(1);
	float min = sensor.getMin(b);
	float max = sensor.getMax(a);
	float angle = sensor.getAngle(0);
	float oper = sensor[5];

	cout << "Angle of 1. sensor" << angle << endl;
	cout << "---------------------------------" << endl;
	cout << "Maximum range among sensors" << max << endl;
	cout << " Index of maximum value" << a << endl;
	cout << "--------------------------------" << endl;
	cout << "Minimum range among sensors" << min << endl;
	cout << " index of  minimum value" << b << endl;
	cout << "--------------------------------" << endl;
	cout << " Range of 120. sensor " << range << endl;
	cout << "--------------------------------" << endl;
	cout << " operator overload is testing" << oper << endl;
	cout << endl;

	system("pause");
}